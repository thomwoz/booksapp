package pl.akademiakodu.authorAndBookHibernate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.akademiakodu.authorAndBookHibernate.dao.AuthorRepository;
import pl.akademiakodu.authorAndBookHibernate.dao.BookRepository;
import pl.akademiakodu.authorAndBookHibernate.model.Author;
import pl.akademiakodu.authorAndBookHibernate.model.Book;

import java.util.Optional;

@Controller
public class MainController {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    AuthorRepository authorRepository;

    //authors/1(idautora) - ma wyswietlac autora wraz z jego ksiazkami

    @GetMapping("/")
    public String showMainPage(){
        return "index";
    }

    @GetMapping("/addauthor")
    public String addauthor(){
        return "addauthor";
    }

    @PostMapping("/")
    public String addAuthorNotification(@ModelAttribute Author author, ModelMap modelMap){
        modelMap.addAttribute("addedauthor", "Dodano nowego autora.");
        authorRepository.save(author);
        return "index";
    }

    @GetMapping("/add")
    public String add(ModelMap modelMap){
        modelMap.addAttribute("authors", authorRepository.findAll());
        return "form";
    }

    @PostMapping("/create")
    public String create(@ModelAttribute Book book) {
        bookRepository.save(book);
        return "create";
    }

    @GetMapping("/author")
    public String showAuthors(ModelMap modelMap){
        modelMap.put("authors", authorRepository.findAll());
        return "author";
    }

    @GetMapping("/author/{id}")
    public String showAuthorsBooks(@PathVariable Long id, ModelMap modelMap){
        modelMap.addAttribute("author", authorRepository.findById(id).get());
        modelMap.addAttribute("books",bookRepository.findByAuthor(authorRepository.findById(id).get()));
        return "books";
    }

}
