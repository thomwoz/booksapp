package pl.akademiakodu.authorAndBookHibernate.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.authorAndBookHibernate.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
