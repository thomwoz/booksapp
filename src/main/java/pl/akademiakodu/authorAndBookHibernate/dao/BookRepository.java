package pl.akademiakodu.authorAndBookHibernate.dao;

import org.springframework.data.repository.CrudRepository;
import pl.akademiakodu.authorAndBookHibernate.model.Author;
import pl.akademiakodu.authorAndBookHibernate.model.Book;

import java.util.List;


public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findByAuthor(Author author);
}
