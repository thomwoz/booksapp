package pl.akademiakodu.authorAndBookHibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorAndBookHibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorAndBookHibernateApplication.class, args);
	}

}

